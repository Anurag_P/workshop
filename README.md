# workshop
https://docs.google.com/document/d/14jf5m6zA7iFFsd6bMejmkkYLcCp2N9UidaUn6-K42_Q/edit

#competition type
https://freeonlinesurveys.com/s/QCmg3ari

# AI feedback-
https://freeonlinesurveys.com/s/BLPQqQbo

# yesterday's coding
https://freeonlinesurveys.com/s/D5kPu5ep

# yolo predictions
https://freeonlinesurveys.com/s/T67Lc2tG

# image submissions
https://docs.google.com/spreadsheets/d/1pYIN6KqNUXoQgVsZgZE6D6PVSK65NaLyxU138M3w9sM/edit#gid=0

# more experiments
## create from text
https://experiments.runwayml.com/generative_engine/

## create from sketch
https://affinelayer.com/pixsrv/


# to run cart pole
python test.py

# to run hide and seek
### go the folder multi-agent-emergence-environments
bin/examine.py examples/hide_and_seek_quadrant.jsonnet examples/hide_and_seek_quadrant.npz




# workshop feedback
https://freeonlinesurveys.com/s/oZA6Bt7l